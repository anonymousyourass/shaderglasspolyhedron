Shader "Unlit/RaymarchGlass"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"



#define DELTA				0.001
#define RAY_COUNT			7
#define RAY_LENGTH_MAX		100.0
#define RAY_STEP_MAX		100
#define LIGHT				fixed3 (1.0, 1.0, -1.0)
#define REFRACT_FACTOR		0.6
#define REFRACT_INDEX		1.6
#define AMBIENT				0.2
#define SPECULAR_POWER		3.0
#define SPECULAR_INTENSITY	0.5
#define FADE_POWER			1.0
#define M_PI				3.1415926535897932384626433832795
#define GLOW_FACTOR			1.5
#define LUMINOSITY_FACTOR	2.0

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 ro : TEXCOORD1;
				float3 hitPos : TEXCOORD2;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				o.ro = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos,1));
				o.hitPos = v.vertex;
				
				return o;
			}

fixed3x3 mRotate (in fixed3 angle) {
	float c = cos (angle.x);
	float s = sin (angle.x);
	fixed3x3 rx = fixed3x3 (1.0, 0.0, 0.0, 0.0, c, s, 0.0, -s, c);

	c = cos (angle.y);
	s = sin (angle.y);
	fixed3x3 ry = fixed3x3 (c, 0.0, -s, 0.0, 1.0, 0.0, s, 0.0, c);

	c = cos (angle.z);
	s = sin (angle.z);
	fixed3x3 rz = fixed3x3 (c, s, 0.0, -s, c, 0.0, 0.0, 0.0, 1.0);

	return mul(mul(rz,ry), rx);
}

static fixed3 k = fixed3(0,0,0);

float getDistance (in fixed3 p) {

	float repeat = 20.0;
	fixed3 q = p + repeat * 0.5;
	k = floor (q / repeat);
	q -= repeat * (k + 0.5);
	p = mul(mRotate (k) , q);

	float top = p.y - 3.0;
	float angleStep = M_PI / max (2.0, abs (k.x + 2.0 * k.y + 4.0 * k.z));
	float angle = angleStep * (0.5 + floor (atan2 (p.x, p.z) / angleStep));
	float side = cos (angle) * p.z + sin (angle) * p.x - 2.0;
	float bottom = -p.y - 3.0;

	return max (top, max (side, bottom));
}

fixed3 getFragmentColor (in fixed3 origin, in fixed3 direction) {
	fixed3 lightDirection = normalize (LIGHT);
	fixed2 delta = fixed2 (DELTA, 0.0);

	fixed3 fragColor = fixed3 (0.0, 0.0, 0.0);
	float intensity = 1.0;

	float distanceFactor = 1.0;
	float refractionRatio = 1.0 / REFRACT_INDEX;
	float rayStepCount = 0.0;
	for (int rayIndex = 0; rayIndex < RAY_COUNT; ++rayIndex) {

		// Ray marching
		float dist = RAY_LENGTH_MAX;
		float rayLength = 0.0;
		for (int rayStep = 0; rayStep < RAY_STEP_MAX; ++rayStep) {
			dist = distanceFactor * getDistance (origin);
			float distMin = max (dist, DELTA);
			rayLength += distMin;
			if (dist < 0.0 || rayLength > RAY_LENGTH_MAX) {
				break;
			}
			origin += direction * distMin;
			++rayStepCount;
		}

		// Check whether we hit something
		fixed3 backColor = fixed3 (0.0, 0.0, 0.1 + 0.2 * max (0.0, dot (-direction, lightDirection)));
		if (dist >= 0.0) {
			fragColor = fragColor * (1.0 - intensity) + backColor * intensity;
			break;
		}

		// Get the normal
		fixed3 normal = normalize (distanceFactor * fixed3 (
			getDistance (origin + delta.xyy) - getDistance (origin - delta.xyy),
			getDistance (origin + delta.yxy) - getDistance (origin - delta.yxy),
			getDistance (origin + delta.yyx) - getDistance (origin - delta.yyx)));

		// Basic lighting
		fixed3 reflection = reflect (direction, normal);
		if (distanceFactor > 0.0) {
			float relfectionDiffuse = max (0.0, dot (normal, lightDirection));
			float relfectionSpecular = pow (max (0.0, dot (reflection, lightDirection)), SPECULAR_POWER) * SPECULAR_INTENSITY;
			float fade = pow (1.0 - rayLength / RAY_LENGTH_MAX, FADE_POWER);

			fixed3 localColor = max (sin (k * k), 0.2);
			localColor = (AMBIENT + relfectionDiffuse) * localColor + relfectionSpecular;
			localColor = lerp (backColor, localColor, fade);

			fragColor = fragColor * (1.0 - intensity) + localColor * intensity;
			intensity *= REFRACT_FACTOR;
		}

		// Next ray...
		fixed3 refraction = refract (direction, normal, refractionRatio);
		if (dot (refraction, refraction) < DELTA) {
			direction = reflection;
			origin += direction * DELTA * 2.0;
		}
		else {
			direction = refraction;
			distanceFactor = -distanceFactor;
			refractionRatio = 1.0 / refractionRatio;
		}
	}

	// Return the fragment color
	return fragColor * LUMINOSITY_FACTOR + GLOW_FACTOR * rayStepCount / float (RAY_STEP_MAX * RAY_COUNT);
}

			fixed4 frag (v2f i) : SV_Target
			{
                // Define the ray corresponding to this fragment
	            fixed2 frag = (2.0 * i.vertex.xy - _ScreenParams.xy) / _ScreenParams.y;
	            fixed3 direction = normalize (fixed3 (frag.x,frag.y, 2.0));
	            // Set the camera
	            fixed3 origin = fixed3 ((15.0 * cos (_Time.y * 0.1)), 10.0 * sin (_Time.y * 0.2), 15.0 * sin (_Time.y * 0.1));
	            fixed3 forward = -origin;
	            fixed3 up = fixed3 (sin (_Time.y * 0.3), 2.0, 0.0);
	            fixed3x3 rotation;
	            rotation [2] = normalize (forward);
	            rotation [0] = normalize (cross (up, forward));
	            rotation [1] = cross (rotation [2], rotation [0]);
	            direction = mul(rotation ,direction);

	            // Set the fragment color
	            fixed4 col = fixed4 (getFragmentColor (origin, direction), 1.0);


				return col;
			}
			ENDCG
		}
	}
}
